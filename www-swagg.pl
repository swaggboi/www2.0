#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Mar 2021

use Mojolicious::Lite;
use Regexp::Common qw{net};
use Digest::SHA qw{sha1_hex};
use Number::Format qw{format_number};

plugin 'Config';

# CGI scripts
plugin CGI => ['/cgi-bin/guest.cgi', './cgi-bin/guest_mm.cgi'];
plugin CGI => ['/cgi-bin/whoami.cgi',  './cgi-bin/whoami.cgi'];

under sub {
    my ($c)         = @_;
    my $sessionLife = 604800;
    my $hostHeader  = $c->req->headers->host();

    if ($c->cookie('banner') eq 'seen') {
        # Set session for a week
        $c->session(expiration => $sessionLife, banner => 'seen');
        # Kill plain-text cookie
        $c->cookie(banner => 'seen', {expires => 1});
    }

    # Pass in the expiration for plain-text cookie
    $c->stash(sessionLife => $sessionLife);

    # Opt out of Google FLoC
    # https://paramdeo.com/blog/opting-your-website-out-of-googles-floc-network
    $c->res->headers->header('Permissions-Policy', 'interest-cohort=()');

    1;
};

# The main landing page; index.html
get '/', sub {
    my ($c)     = @_;
    my $count   = format_number time; # Grab epoch and add commas
    my $fortune = `/usr/games/fortune` || `fortune` || "huh??\n";

    $c->stash(
        count   => $count,
        fortune => $fortune
        );

    $c->render();
}, 'index';

# Process mac address to ula6 ala the ol' ula6.cgi script:
# - Current time of day in 64-bit NTP format
# - Obtain EUI64
# - Cat first with second
# - Compute SHA-1 of third and use least significant 40 bits
# TODO: This ancient code could certainly be cleaned up
get '/ula6', sub {
    my ($c) = @_;
    my ($mac, $ula6);

    # Check the MAC
    $mac = $c->param('macaddr') ? lc $c->param('macaddr') : '';
    if ($mac =~ /$RE{net}{MAC}/) {
        # Local vars for this bit
        my (
            $binfield,
            $decfield,
            $digesty,
            $epoch,
            @fields,
            $fulleui,
            $halfone,
            $halftwo,
            $hexfield,
            $uniqueid
            );

        # EUI64
        @fields   = split(/:/, $mac);
        $halftwo  = "fe" . $fields[3] . $fields[4] . $fields[5];
        $binfield = sprintf "%b", hex $fields[0];
        $decfield = sprintf "%d", hex $fields[0];
        $decfield = ($binfield =~ /1[01]$/) ? $decfield - 2 : $decfield + 2;
        $hexfield = sprintf "%x", int $decfield;
        $halfone  = $hexfield . $fields[1] . $fields[2] . "ff";
        $fulleui  = $halfone  . $halftwo;

        # Current time of day
        $epoch = time() + 2208988800;

        # Cat with the time
        $digesty   = sha1_hex($epoch . $fulleui);
        ($uniqueid = "fd" . substr $digesty, -10) =~ s/(.{4})/$1:/g;

        # Set the ULA
        $ula6 = $uniqueid . ':/48';
    }

    $c->render(ula6 => $ula6);
};

# Reset session cookie
get '/reset', sub {
    my ($c) = @_;

    $c->session(expires => 1);
    $c->redirect_to('index');
};

# Default route
get '/:route', [route => [qw{die me news portal}]], sub {
    my ($c) = @_;

    $c->render(template => $c->stash('route'));
};

# Send it
app->secrets(app->config->{secrets}) || die $!;
app->start();
