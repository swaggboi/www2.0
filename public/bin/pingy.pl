#!/usr/bin/env perl
#
# danielbowling424@msn.com
# http://swagg.cc
#
# Copyright 2019 Daniel Bowling, Mount Rainier, Maryland, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use Net::Ping; # https://perldoc.perl.org/Net/Ping.html
use Regexp::Common qw(net); # Net::Ping must be up-to-date for IPv6!!

my $p4 = Net::Ping->new("icmp");
my $p6 = Net::Ping->new("icmpv6");
my $svmgr = "systemctl";
my $svact = "restart";
my @svcs = ("zebra", "ospfd", "ospf6d");
my $sleepy = 9;

sub rest_svc {
    print "not reachable!!\n$svact @svcs... ";
    system($svmgr, $svact, @svcs);
    print "Done!!\nwaiting $sleepy seconds to cont... ";
    sleep ($sleepy);
    print "Done!!\n";
}

sub ping_ip {
    if ($_ =~ m/$RE{net}{IPv4}/) {
        print "pinging $_ via ICMP... ";
        if ($p4->ping($_, 2)) {
            print "success!!\n";
        } else {
            rest_svc();
        }
    } elsif ($_ =~ m/$RE{net}{IPv6}/) {
        print "pinging $_ via ICMPv6... ";
        if ($p6->ping($_, 2)) {
            print "success!!\n";
        } else {
            rest_svc();
        }
    } 
}

foreach (@ARGV) {
    if ($_ =~ m/[A-F]*/) {
        $_ =~ tr/[A-F]/[a-f]/; # toggle case
    }
    ping_ip($_);
}

$p4->close();
$p6->close();
