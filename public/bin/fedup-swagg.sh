#!/bin/sh
#
# this script is based on https://fedoraproject.org/wiki/DNF_system_upgrade
# danielbowling424@msn.com
# http://swagg.ddns.net
#
# Consider this quick n' dirty
# If your system breaks, you may keep both pieces
#
# Copyright 2019 Daniel Bowling, Mount Rainier, Maryland, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

DNFY='/usr/bin/dnf --allowerasing --assumeyes'
FVER="`cat /etc/fedora-release | grep --only-matching "[0-9]*"`"

read -p "This script will trigger a reboot!! Proceed ? (Y/N): "  ANSW

case $ANSW in
    Y|y)
	break
	;;
    *)
	echo 'Goodbye'
	exit 64
	;;
esac

$DNFY upgrade --refresh
$DNFY install dnf-plugin-system-upgrade
$DNFY system-upgrade download --refresh --releasever=`expr $FVER + 1`
$DNFY system-upgrade reboot
