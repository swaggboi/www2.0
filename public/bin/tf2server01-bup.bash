#!/bin/bash
#
# danielbowling424@msn.com
# http://swagg.ddns.net
#
# Copyright 2019 Daniel Bowling, Mount Rainier, Maryland, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SU="/bin/su"
TAR="/bin/tar"
GPG="/bin/gpg"
DATE="/bin/date +%m%d%Y"
RM="/bin/rm --force"
LSLH="/bin/ls --human-readable -l"
MUTT="/bin/mutt"
MEMAIL="danielbowling424@msn.com"
GDRV="/root/bin/gdrive-linux-x64"
GDIR="1wToZq_W4Oq-XBzkpdmGObVaaJ3pR3XIx"
GRP="/bin/grep"

# If backups directory doesn't exist, create it:

  if ! [[ -d /home/tf2server/backups ]] ; then
    $SU - tf2server '--command=mkdir /home/tf2server/backups'
  fi

# Stop tf2server then tar the tf2server01 directory

$SU - tf2server '--command=/home/tf2server/tf2server01/tf2server stop'
$SU - tf2server "--command=$TAR -czf /home/tf2server/backups/tf2server01-backup-`$DATE`.tar.gz \
  /home/tf2server/tf2server01"

# Start tf2server then gpg the tarball

$SU - tf2server '--command=/home/tf2server/tf2server01/tf2server start'
$SU - tf2server "--command=$GPG --no-tty --encrypt --output=/home/tf2server/backups/tf2server01-backup-`$DATE`.tar.gz.gpg \
  --recipient=lysolbrand /home/tf2server/backups/tf2server01-backup-`$DATE`.tar.gz"

# Upload encrypted tarball to gdrive

$GDRV upload --parent $GDIR /home/tf2server/backups/tf2server01-backup-`$DATE`.tar.gz.gpg

# Begin clean-up

local-clean () {
	(
	/bin/ls /home/tf2server/backups | \
	$GRP --invert-match "tf2server01-backup-`/bin/date +%m`......\.tar\.gz\.gpg*"
	)
}

  for i in $( local-clean ) ; do
    $SU - tf2server "--command=$RM /home/tf2server/backups/$i"
  done

gdrive-clean () {
	(
	$GDRV list --query " '$GDIR' in parents" --no-header | \
	$GRP -v "tf2server01-backup-`$DATE`.tar.gz.gpg" | \
	$GRP --only-matching '^[[:graph:]{33}]*'
	)
}

  for i in $( gdrive-clean ) ; do
    $GDRV delete $i
  done

$SU - tf2server "--command=$RM /home/tf2server/backups/tf2server01-backup-`$DATE`.tar.gz"

# Send email with status of backup

gdrive-date () {
	(
	$GDRV list --query " '$GDIR' in parents" --no-header | \
	$GRP "tf2server01-backup-`$DATE`.tar.gz.gpg" | \
	$GRP --only-matching "`$DATE`"
	)
}

back-stat () {
	(
	echo 'List of /home/tf2server/backups directory contents:'
	echo ""
	echo "`$LSLH /home/tf2server/backups`"
	echo ""
	echo 'List of gdrive /backups/tf2server directory contents:'
	echo ""
	echo "`$GDRV list --query " '$GDIR' in parents"`"
	)
}
 
  if [[ `gdrive-date` != `$DATE` ]] ; then 
    back-stat | \
    $MUTT -s 'Backup Failed!!' "$MEMAIL"
    exit 64
  fi

back-stat | \
$MUTT -s 'Backup Successful' "$MEMAIL"
exit 0
