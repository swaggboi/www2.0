#!/bin/sh
#
# danielbowling424@msn.com
# http://swagg.ddns.net
#
# Copyright 2019 Daniel Bowling, Mount Rainier, Maryland, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PWRT="`date -d "+10min" +%R`"
WLLN="wall --nobanner"
DNFY="/usr/bin/apt-get --assume-yes"

if [ -z $1 ] ; then
  echo "usage: $0 `whoami`"
  exit 64
fi

until [ "`date +%R`" = "$PWRT" ] ; do
  for i in $* ; do
    if [ `users | cut -f 1 -d " "` = "$i" ] ; then
      $WLLN "$0: user `users | cut -f 1 -d " "` logged in, pending shutdown cancelled"
      exit 0
    fi
  done
  $WLLN "$0: system will shutdown at $PWRT due to inactivity"
  sleep 30
done

$WLLN "$0: commencing system maintenance & shutdown"
$DNFY update | $WLLN
$DNFY dist-upgrade | $WLLN
$DNFY autoclean | $WLLN
$DNFY autoremove | $WLLN

systemctl poweroff
