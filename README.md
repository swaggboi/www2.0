# www2.0

The successor to my homepage. Hold on to your butts.

## Building/Testing

### Build the container locally

    podman build -t www-swagg .

Tests are performed during the build process

## Tagging/Pushing

### Tag the image

    podman tag www-swagg \
        git.seriousbusiness.international/swaggboi_priv/www-swagg

### Send it

    podman push git.seriousbusiness.international/swaggboi_priv/www-swagg

### Pull it

    podman pull git.seriousbusiness.international/swaggboi_priv/www-swagg

### Run it

    podman run -dt --rm --name www-swagg -p 3000:3000 www-swagg:latest

### Create unit file

    podman generate systemd --files --new --name www-swagg
