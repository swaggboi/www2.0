FROM docker.io/perl:5.36

# Move it
WORKDIR /opt
COPY cgi-bin/ ./cgi-bin/
COPY public/ ./public/
COPY t/ ./t/
COPY templates/ ./templates/
COPY cpanfile .
COPY www-swagg.conf .
COPY www-swagg.pl .

# Dependency time
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install fortune-mod ruby-dev fortunes-bofh-excuses \
    fortunes-mario fortunes-min fortunes
RUN cpanm --installdeps .
RUN gem install cgi

# Test it
#RUN prove -l -v

# Finish setting up the environment
ENV MOJO_REVERSE_PROXY=1
ENV SERVER_ADMIN="swaggboi@slackware.uk"
EXPOSE 3000

# Send it
CMD ["perl", "www-swagg.pl", "prefork", "-m", "production"]
