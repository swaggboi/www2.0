#!/usr/bin/env perl

# Guestbook CGI Program
# Daniel Bowling <swaggboi@slackware.uk>
# Sep 2020

use strict;
use warnings;
use utf8;
use open qw{:std :utf8}; # Fix "Wide character in print" warning
use CGI qw{-utf8};       # Needed to parse unicode params
use CGI::Carp qw{fatalsToBrowser};

print CGI->redirect('https://guestbook.swagg.net');
