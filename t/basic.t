#!/usr/bin/env perl

use Test::More;
use Mojo::File qw{curfile};
use Test::Mojo;

my $script     = curfile->dirname->sibling('www-swagg.pl');
my $t          = Test::Mojo->new($script);
my @routes     = ('', 'ula6', 'die', 'me', 'news', 'portal');
my %guest_form = (
    name     => 'AliceTug',
    location => 'Reston, VA',
    message  => 'Ayy... lmao'
    );

# Routes
for my $route (@routes) {
    $t->get_ok("/$route")->status_is(200)
}

# CGI Scripts
$t->get_ok('/cgi-bin/whoami.cgi')->status_is(200);
$t->get_ok('/cgi-bin/guest.cgi' )->status_is(302);

# ULA Tool
$t->get_ok('/ula6', form => {macaddr => 'ea:88:5e:3f:a3:34'})->status_is(200);

# Resume files
$t->get_ok('/resume/dbowling-resume.odt')->status_is(200);
$t->get_ok('/resume/dbowling-resume.docx')->status_is(200);

done_testing();
